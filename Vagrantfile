# -*- mode: ruby -*-
# vi: set ft=ruby :

# Ensure Docker contains don't all start up at the same time because they depend on each other.
ENV['VAGRANT_NO_PARALLEL'] = 'yes'

# Set Docker as default provider
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'docker'

Vagrant.require_version ">= 1.6"

Vagrant.configure("2") do |config|
  # By default, Vagrant 1.7+ automatically inserts a different
  # insecure keypair for each new VM created. The easiest way
  # to use the same keypair for all the machines is to disable
  # this feature and rely on the legacy insecure key.
  config.ssh.insert_key = false

  # Docker Host VM
  # Must be configured before the default docker provider configuration.
  config.vm.define "docker", primary: true, autostart: false do |v|
    v.vm.hostname = "zym.dev"

    # Every Vagrant virtual environment requires a box to build off of.
    v.vm.box       = "phusion/ubuntu-14.04-amd64"

    # The url from where the 'config.vm.box' box will be fetched if it
    # doesn't already exist on the user's system.
    # config.vm.box_url   = "http://files.vagrantup.com/precise64.box"

    # Boot with a GUI so you can see the screen. (Default is headless)
    # config.vm.boot_mode = :gui

    # Assign this VM to a bridged network, allowing you to connect directly to a
    # network using the host's network device. This makes the VM appear as another
    # physical device on your network.
    # config.vm.network :bridged

    # Forward a port from the guest to the host, which allows for outside
    # computers to access the VM, whereas host only networking does not.
    # config.vm.forward_port 80, 8080

    # Assign this VM to a host-only network IP, allowing you to access it
    # via the IP. Host-only networks can talk to the host machine as well as
    # any other machines on the same network, but cannot be accessed (through this
    # network interface) by any external networks.
    v.vm.network :private_network, ip: "192.168.33.2"

    # Share an additional folder to the guest VM. The first argument is
    # an identifier, the second is the path on the guest to mount the
    # folder, and the third is the path on the host to the actual folder.
    v.vm.synced_folder ".", "/vagrant", :nfs => !RUBY_PLATFORM.downcase.include?("w32"), :nfs_udp => false, :mount_options => ['rw', 'fsc' ,'actimeo=2']

    # Default virtualbox provider configuration.
    v.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--chipset", 'ich9']
      vb.customize ["modifyvm", :id, "--memory", 1024]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
      vb.customize ["modifyvm", :id, "--ioapic", "on"]
      vb.customize ["storageattach", :id, "--storagectl", "SATA Controller", "--nonrotational", "on", "--port", "0"]

      # Docker context transfer during builds are painfully slow without using the virtio adapter
      vb.customize ["modifyvm", :id, "--nictype1", "virtio"]
      vb.customize ["modifyvm", :id, "--nictype2", "virtio"]

      # Virtualbox is not great with symlinks, so if there are symlinks in your
      # share, you'll want to add this
      vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant-root", "1"]
    end

    config.vm.provision "shell" do |s|
      s.inline = "command -v docker >/dev/null 2>&1 || (sudo apt-get update && \
                    sudo apt-get install curl -y && \
                    curl -sSL https://get.docker.com/ | sudo sh && \
                    sudo usermod -a -G docker vagrant && \
                    sudo chown vagrant:vagrant /var/lib/docker) && \
                    ps aux | grep 'sshd:' | awk '{print $2}' | xargs kill"
    end

    # Doesn't seem to work properly at the moment.
    #v.vm.provision "docker" do |d|
    #end
  end

  # Default docker provider configuration.
  config.vm.provider :docker do |d|
    d.force_host_vm       = true
    d.vagrant_machine     = "docker"
    d.vagrant_vagrantfile = __FILE__

    # Increaes performance of builds due to speeding up the build context transfer
    # Vagrant bug with nfs mount is busy or mounted, so commenting out for now
    #d.host_vm_build_dir_options = {
    #    type: !RUBY_PLATFORM.downcase.include?("w32") ? "nfs" : "smb"
    #}

    d.env = {
        MAINTENANCE: "0",   # Enable or disable maintenance mode (Displays maintenance page for all requests)
        ENVIRONMENT: "dev",

        DB_DRIVER:   "pdo_mysql",
        DB_HOST:     "mysql",
        DB_USER:     "root",
        DB_PASSWORD: "",
        DB_NAME:     "zym",

        ELASTICSEARCH_HOST: "elasticsearch",
        ELASTICSEARCH_PORT: "9200",

        MAIL_TRANSPORT:  "mail",
        MAIL_ENCRYPTION: "",
        MAIL_AUTH_MODE:  "login",
        MAIL_HOST:       "127.0.0.1",
        MAIL_PORT:       "25",
        MAIL_USERNAME:   "",
        MAIL_PASSWORD:   "",

        REDIS_HOST:     "redis",
        REDIS_PORT:     "6379",
        REDIS_DATABASE: "0"
    }
  end

  config.vm.define "mysql" do |v|
    v.vm.provider :docker do |d|
      d.image     = "mysql"
      d.name      = "mysql"
      d.expose    = ["3306"]
      d.ports     = ["3306:3306"]

      d.env = {
          MYSQL_ROOT_PASSWORD: "",
          MYSQL_ALLOW_EMPTY_PASSWORD: "yes",
          MYSQL_DATABASE: "zym"
      }
    end
  end

  config.vm.define "elasticsearch" do |v|
    v.vm.provider :docker do |d|
      d.image     = "elasticsearch:1.5"
      d.name      = "elasticsearch"
      d.volumes   = [
        "/vagrant/var/docker/elasticsearch/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml",

        # Issue with Elasticsearch unable to chown directory when running under boot2docker if container does not
        # have the proper permissions
        #"/vagrant/var/indexes/elasticsearch:/usr/share/elasticsearch/data"
      ]
    end
  end

  config.vm.define "redis" do |v|
    v.vm.provider :docker do |d|
      d.image     = "redis"
      d.name      = "redis"
    end
  end

  config.vm.define "symfony" do |v|
    v.vm.provider :docker do |d|
      d.build_dir = "."
      d.name      = "symfony"
      d.remains_running = false
      d.ports     = ["80:80", "443:443", "9000:9000"]
      d.volumes = [
        "/vagrant:/srv",
        "/vagrant/var/docker/entrypoint.sh:/entrypoint.sh",
        "/vagrant/var/docker/cron/cron.d/:/etc/cron.d/"
      ]

      d.link "elasticsearch:elasticsearch"
      d.link "redis:redis"
      d.link "mysql:mysql"
    end
  end
end
