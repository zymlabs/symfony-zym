#!/bin/bash
#
# Docker Entrypoint
#
# Configures the application
# Runs nginx by default

# Default ENV variables
: ${ENVIRONMENT:="prod"}
: ${MAINTENANCE:="0"}
: ${HTTP_CACHE:="0"}
: ${TRUSTED_HOSTS:=""}
: ${TRUSTED_PROXIES:="127.0.0.1"}
: ${NEWRELIC_LICENSE:=""}
: ${NEWRELIC_APPNAME:="PHP Application"}
: ${NGINX_SERVER_NAME:="zym.dev"}
: ${NGINX_SYMFONY_HOST:="127.0.0.1"}
: ${NGINX_SYMFONY_PORT:="9000"}
: ${SYMFONY2_AUTO_MIGRATION:="0"}

: ${MAIL_TRANSPORT:="mail"}
: ${MAIL_ENCRYPTION:=""}
: ${MAIL_AUTH_MODE:="login"}
: ${MAIL_HOST:="127.0.0.1"}
: ${MAIL_PORT:="25"}
: ${MAIL_USERNAME:=""}
: ${MAIL_PASSWORD:=""}

: ${ELASTICSEARCH_HOST:="127.0.0.1"}
: ${ELASTICSEARCH_PORT:="9200"}

: ${COMPOSER_ALLOW_SUPERUSER:="1"}

export ENVIRONMENT
export MAINTENANCE
export HTTP_CACHE
export TRUSTED_HOSTS
export TRUSTED_PROXIES

export NEWRELIC_LICENSE
export NEWRELIC_APPNAME

export NGINX_SERVER_NAME
export NGINX_SYMFONY_HOST
export NGINX_SYMFONY_PORT

export SYMFONY_AUTO_MIGRATION

export MAIL_TRANSPORT
export MAIL_ENCRYPTION
export MAIL_AUTH_MODE
export MAIL_HOST
export MAIL_PORT
export MAIL_USERNAME
export MAIL_PASSWORD

export ELASTICSEARCH_HOST
export ELASTICSEARCH_PORT

<<<<<<< HEAD
export COMPOSER_ALLOW_SUPERUSER

echo "Zym Labs Symfony2 Container"
=======
echo "Zym Labs Symfony Container"
>>>>>>> master
echo ""
echo "Usage:"
echo "    command [options] [arguments]"
echo ""
echo "The default behavior of this container is to start basic services with supervisor."
echo "It is the same as running:"
echo "    docker run zymlabs/symfony-zym supervisord"
echo ""
echo "Supported commands are:"
echo "    bash"
echo "    cron -f"
echo "    nginx"
echo "    php-fpm7.0"
echo "    supervisord"
echo ""
echo "Support environment variables are:"
echo "    ELASTICSEARCH_HOST=${ELASTICSEARCH_HOST} - Elasticsearch host. (127.0.0.1)"
echo "    ELASTICSEARCH_PORT=${ELASTICSEARCH_PORT} - Elasticsearch port. (9200)"
echo "    ENVIRONMENT=${ENVIRONMENT} - Symfony2 environment to use (prod, dev, test)"
echo "    MAINTENANCE=${MAINTENANCE} - Currently makes nginx display maintenance page. (0,1)"
echo "    HTTP_CACHE=${HTTP_CACHE} - Enables Symfony2 built in HTTP cache. (0,1)"
echo "    TRUSTED_HOSTS=${TRUSTED_HOSTS} - Trusted HTTP hosts header to answer to. (example.com)"
echo "    TRUSTED_PROXIES=${TRUSTED_PROXIES} - IP ranges of proxies to trust. (127.0.0.1/32)"
echo "    MAIL_TRANSPORT=${MAIL_TRANSPORT} - Mail transport. (mail)"
echo "    MAIL_ENCRYPTION=${MAIL_ENCRYPTION} - Mail encryption type."
echo "    MAIL_AUTH_MODE=${MAIL_AUTH_MODE} - Mail authentication mode. (0,1)"
echo "    MAIL_HOST=${MAIL_HOST} - Mail server host. (127.0.0.1)"
echo "    MAIL_PORT=${MAIL_PORT} - Mail server port. (25)"
echo "    MAIL_USERNAME=${MAIL_USERNAME} - Mail server username."
echo "    MAIL_PASSWORD=${MAIL_PASSWORD} - Mail server password."
echo "    NEWRELIC_LICENSE=${NEWRELIC_LICENSE} - License to use with NewRelic"
echo "    NEWRELIC_APPNAME=${NEWRELIC_APPNAME} - App name to use with NewRelic (PHP Application)"
echo "    NGINX_SERVER_NAME=${NGINX_SERVER_NAME} - Enables Symfony2 to trust proxies. (0,1)"
echo "    NGINX_SYMFONY_HOST=${NGINX_SYMFONY_HOST} - Nginx fastcgi server host for Symfony. (127.0.0.1)"
echo "    NGINX_SYMFONY_PORT=${NGINX_SYMFONY_PORT} - Nginx fastcgi server port for Symfony. (80)"
echo "    SYMFONY_AUTO_MIGRATION=${SYMFONY_AUTO_MIGRATION} - Enables auto running of Symfony migrations. (0,1)"
echo ""

# Check if environment is dev
if [ "$ENVIRONMENT" == "dev" ]
then
    # PHP Settings
    sed -i 's/display_errors = Off/display_errors = On/' /etc/php/7.0/cli/php.ini
    sed -i 's/display_errors = Off/display_errors = On/' /etc/php/7.0/fpm/php.ini

    # Enable xdebug
    echo "zend_extension=xdebug.so"            > "/etc/php/7.0/mods-available/xdebug.ini"
    echo "xdebug.cli_color = 1"               >> "/etc/php/7.0/mods-available/xdebug.ini"
    echo "xdebug.remote_connect_back = 1"     >> "/etc/php/7.0/mods-available/xdebug.ini"
    echo "xdebug.coverage_enable = 0"         >> "/etc/php/7.0/mods-available/xdebug.ini"
    echo "xdebug.profiler_enable_trigger = 1" >> "/etc/php/7.0/mods-available/xdebug.ini"
    echo "xdebug.coverage_enable = 1"         >> "/etc/php/7.0/mods-available/xdebug.ini"
else
    # Disable xdebug
    echo ";zend_extension=xdebug.so" > "/etc/php/7.0/mods-available/xdebug.ini"

    # Disable assetic-watch
    if [ -f /etc/supervisor/conf.d/assetic.conf ]; then
        rm /etc/supervisor/conf.d/assetic.conf
    fi
fi

# Ensure proper permissions for Symfony
chmod -R 775 $PROJECT_DIR/var/cache
chmod -R 775 $PROJECT_DIR/var/logs

# Generate configuration by looping through .dist files in config/
for f in $PROJECT_DIR/config/*.dist
do
    # Replace ${VAR} with variables set in environment.
    # foo.xml.dist will have vars replaced and created as foo.xml
    #
    # $ x="/foo/fizzbuzz.bar.quux"
    # $ y=${x%.*}
    # $ echo $y
    # /foo/fizzbuzz.bar
    perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' $f > ${f%.*}
done

# Generate configuration by looping through .dist files in /etc/nginx/conf.d/default.conf
for f in /etc/nginx/conf.d/*.conf.dist
do
    # Replace ${VAR} with variables set in environment.
    # foo.xml.dist will have vars replaced and created as foo.xml
    #
    # $ x="/foo/fizzbuzz.bar.quux"
    # $ y=${x%.*}
    # $ echo $y
    # /foo/fizzbuzz.bar
    perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' $f > ${f%.*}
done

# Generate configuration by looping through .dist files in /etc/php/7.0/mods-available
for f in /etc/php/7.0/mods-available/*.ini.dist
do
    # Replace ${VAR} with variables set in environment.
    # foo.xml.dist will have vars replaced and created as foo.xml
    #
    # $ x="/foo/fizzbuzz.bar.quux"
    # $ y=${x%.*}
    # $ echo $y
    # /foo/fizzbuzz.bar
    perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' $f > ${f%.*}
done

# If ENV MAINTENANCE then enable maintenance mode on Nginx
if [ "$MAINTENANCE" == "1" ]
then
    # Create maintenance file in project dir
    touch $PROJECT_DIR/web/maintenance
else
    # Remove maintenance file in project dir if exists
    if [ -f $PROJECT_DIR/web/maintenance ]
    then
        rm $PROJECT_DIR/web/maintenance
    fi
fi

# bootstrap.php.cache not created... Probably the first time, lets run composer install
if [ ! -f $PROJECT_DIR/app/bootstrap.php.cache ]; then
    echo "Project not bootstrapped, bootstrapping project..."
    composer install
fi

# Run Symfony migrations
if [ "$SYMFONY_AUTO_MIGRATION" == "1" ]
then
    bin/console doctrine:migrations:migrate --no-interaction
fi

# Hack to get cron to work
sleep 60 && touch /etc/cron.d/* &

# Run CMD from docker
exec "$@"
