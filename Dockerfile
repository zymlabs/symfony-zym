FROM debian:jessie
MAINTAINER Geoffrey Tran <geoffrey.tran@gmail.com>

# Disable user prompts
ENV DEBIAN_FRONTEND noninteractive

# App install directory
ENV PROJECT_DIR /srv

# Set node environment to production to prevent installing dev dependencies
ENV NODE_ENV production

# Update the repository sources list
RUN \
    apt-get update -q && \
    apt-get install --no-install-recommends -qy wget ca-certificates && \

    # PHP7.0 Dotdeb repository
    sh -c 'echo deb http://packages.dotdeb.org jessie all >> /etc/apt/sources.list.d/dotdeb.list' && \
    wget --quiet -O- https://www.dotdeb.org/dotdeb.gpg | apt-key add - && \

    # New Relic repository
    sh -c 'echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list' && \
    wget --quiet -O- https://download.newrelic.com/548C16BF.gpg | apt-key add - && \

    apt-get update -q && \
    apt-get install --no-install-recommends -qy wget ca-certificates python-setuptools \
        supervisor sendmail-bin htop curl nano git zip unzip mysql-client netcat redis-tools \
        php7.0-fpm php7.0-cli php7.0-curl php7.0-gd php7.0-json php7.0-ldap php7.0-mysql \
        php7.0-odbc php7.0-pgsql php7.0-pspell php7.0-readline php7.0-sqlite php7.0-tidy \
        php7.0-xmlrpc php7.0-xsl php7.0-intl php7.0-apcu php7.0-apcu-bc php7.0-mcrypt php7.0-mbstring \
        php7.0-memcache php7.0-memcached php7.0-redis php7.0-mongo php7.0-xdebug phpunit && \

    apt-get install --no-install-recommends -qy cron && \

    apt-get install --no-install-recommends -qy nginx && \
    echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
    chown -R www-data:www-data /var/lib/nginx && \

    apt-get install --no-install-recommends -qy newrelic-php5  && \

    # NPM and Node.js
    apt-get install --no-install-recommends -qy nodejs npm && \
    npm install --loglevel warn -g less && \
    npm install --loglevel warn -g uglify-js && \
    npm install --loglevel warn -g uglifycss && \
    npm install --loglevel warn -g autoprefixer@5.2 && \
    npm install --loglevel warn -g bower && \
    ln -s /usr/local/bin/autoprefixer /usr/bin/autoprefixer && \
    ln -s /usr/local/bin/uglifycss /usr/bin/uglifycss && \
    ln -s /usr/local/bin/uglifyjs /usr/bin/uglifyjs && \
    ln -s /usr/bin/nodejs /usr/bin/node && \

    # Remove apt-get filse to save space
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \

    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy cron files
COPY var/docker/cron/cron.d/ /etc/cron.d/

# Copy php.ini
COPY var/docker/php.ini /etc/php/7.0/cli/php.ini
COPY var/docker/php.ini /etc/php/7.0/fpm/php.ini

# Copy php7.0 modules config
COPY var/docker/php/mods-available/* /etc/php/7.0/mods-available/

# Copy php-fpm
COPY var/docker/php-fpm.conf /etc/php/7.0/fpm/php-fpm.conf

# Copy nginx config
COPY var/docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY var/docker/nginx/conf.d/default.conf.dist /etc/nginx/conf.d/default.conf.dist
COPY var/docker/nginx/html /usr/share/nginx/html

# Copy supervisor config
COPY var/docker/supervisor/conf.d /etc/supervisor/conf.d/

# Copy the project to the Docker project directory
# Do this as late as possible as it will most likely not be cached on subsequent builds
COPY . ${PROJECT_DIR}

# Change the working directory to the project
WORKDIR ${PROJECT_DIR}

# Composer allow root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Install dependencies and remove .git folders
RUN rm -fr var/cache/* && rm -fr var/log/* \
    && composer install -o --no-interaction --prefer-dist --no-progress \
    && find vendor/ -type d -name .git -exec rm -rf {} +

# Set assets version
RUN GITHASH=`git rev-parse --short HEAD` sh -c  'sed -i "s/assets.version\(.*\)=\(.*\)/assets.version\1= $GITHASH/" config/parameters.ini.dist' && \
    # Remove .git directory
    rm -fr .git

# Install assets
RUN php app/console assets:install --env=prod --no-debug && php app/console assetic:dump --env=prod --no-debug \
    # Clear app caches and logs
    && rm -fr var/cache/* && rm -fr var/log/* \
    # Forward Nginx request and error logs to docker log collector
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && rm -fr /etc/nginx/sites-enabled/default

EXPOSE 9000 80 443

VOLUME /srv

# Copy entrypoint
COPY var/docker/entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

# Run cron
#CMD ["cron", "-f"]

# Run php7.0-fpm
#CMD ["php-fpm7.0", "-F"]

# Run nginx
#CMD ["nginx"]

# Run supervisord which should run the basic services we need (nginx/php7.0-fpm) locally
CMD ["supervisord"]
