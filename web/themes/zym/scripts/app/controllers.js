angular.module('app')
    .controller('NavbarCtrl', ['$scope', '$state', '$mdMedia', '$mdSidenav', '$mdUtil', 'AuthService',
        function($scope, $state, $mdMedia, $mdSidenav, $mdUtil, AuthService){
            $scope.$mdMedia = $mdMedia;
            $scope.isCollapsed = true;
            $scope.user = AuthService.getCurrentUser();

            $scope.toggleSidebar = buildToggler('sidebar');

            $scope.logout = function(){
                AuthService.logout()
                    .then(function(result){
                        $state.go('login');
                    })
                ;
            };

            /**
             * Build handler to open/close a SideNav; when animation finishes
             * report completion in console
             */
            function buildToggler(navID) {
                var debounceFn =  $mdUtil.debounce(function(){
                    $mdSidenav(navID)
                        .toggle();
                }, 300);

                return debounceFn;
            }

            $scope.$on('hitmands.auth:update', function(event) {
                $scope.user = AuthService.getCurrentUser();
            });
        }
    ])
;
