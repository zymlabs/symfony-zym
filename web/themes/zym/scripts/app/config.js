angular.module('app')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {
        $locationProvider
            .html5Mode(true)
            .hashPrefix('!');

        $urlRouterProvider
            // Ignore Symfony2 debugging urls
            .when(/^\/(_(profiler|wdt|error))/, ['$match', '$window',  function ($match, $window) {
                // Load the url and bypass the router
                $window.location.href = $match.input;
            }])

        // For any unmatched url, redirect to /state1
        //$urlRouterProvider.otherwise("/login");

        // Now set up the states
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: Routing.generate('homepage', { '_format': 'ajax' })
            })
        ;
    }])

    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }])

    .config(['gravatarServiceProvider', function(gravatarServiceProvider) {
        gravatarServiceProvider.defaults = {
          size     : 100,
          'default': 'mm'  // Mystery man as default for missing avatars
        };
    }])

    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }])

    // Load Angular Material Icons
    .config(function($mdIconProvider) {
        $mdIconProvider.fontSet('fa', 'FontAwesome');

    })

    // Load Angular Material Theme
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('light-blue')
        ;
    })
;
