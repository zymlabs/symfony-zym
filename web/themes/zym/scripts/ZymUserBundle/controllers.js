angular.module('ZymUserBundle')
    .controller('ResettingRequestCtrl', function($scope, $http, $state, $stateParams) {
        $scope.error = null;
        $scope.success = false;
        $scope.loading = false;

        // Password reset request submit function
        $scope.request = function($event){
            $event.preventDefault();
            $event.stopPropagation();

            $scope.loading = true;
            $scope.error   = null;

            $http({
                method: 'POST',
                url: Routing.generate('zym_user_resetting_request', { '_format': 'json' }),
                data: $.param({ username: this.username }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
                .success(function(data, status){
                    if (data.email !== undefined) {
                        $scope.error   = false;
                        $scope.success = true;
                        $state.go('account.resetting_check_email');
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function(data, status){
                    $scope.error    = data.message;
                    $scope.loading  = false;
                });
        };

        // Clear error message when user starts typing again
        $scope.$watch('username', function(newVal, oldVal){
            if (newVal === oldVal) {
                return;
            }

            $scope.error    = false;
        });
    })

    .controller('ResettingResetCtrl', function($scope, $http, $state, $stateParams){
        $scope.error = null;
        $scope.success = false;
        $scope.loading = false;

        // Password reset submit function
        $scope.submit = function($event){
            $event.preventDefault();
            $event.stopPropagation();

            $scope.loading = true;
            $scope.error   = null;

            $http({
                method: 'POST',
                url: Routing.generate('zym_user_resetting_reset', { 'token': 'reset-token', '_format': 'json' }).replace('reset-token', $stateParams.token),
                data: $.param({
                    'fos_user_resetting_form[plainPassword][first]': this.password,
                    'fos_user_resetting_form[plainPassword][second]': this.confirmPassword
                }),
                headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'XMLHttpRequest'
            }
        })
        .success(function(data, status){
            $scope.error   = false;
            $scope.success = true;
            $state.go('login');
            $scope.loading = false;

            if (Messenger) {
                Messenger().post('Your password has been successfully reset.');
            }
        })
            .error(function(data, status){
                $scope.error    = data.message;
                $scope.loading  = false;
            });
        };
    })

    .controller('LoginCtrl', function($scope, $rootScope, $http, $timeout, $location, AuthService) {
        $scope.error = null;
        $scope.success = false;
        $scope.loading = false;

        $scope.username = '';
        $scope.password = '';
        $scope.rememberMe = false;

        $scope.login = function($event){
            $event.preventDefault();
            $event.stopPropagation();

            $scope.loading = true;
            $scope.error   = null;

            var credentials = $.param({
                _username: this.username,
                _password: this.password,
                _remember_me: this.rememberMe
            });

            $http({
                method: 'POST',
                url: Routing.generate('fos_user_security_check'),
                data: credentials,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
                .success(function(data, status, headers, config, statusText){
                    if (data.redirectUrl !== undefined) {
                        $scope.error   = false;
                        $scope.success = true;

                        // Redirect to target url
                        if (/^(?:[a-z]+:)?\/\//i.test(data.redirectUrl)) {
                            // Redirect URL provided is absolute rather than relative.
                            var parser = document.createElement('a');
                            parser.href = data.redirectUrl;

                            $location.url(parser.pathname + parser.search + parser.hash);
                        } else {
                            $location.url(data.redirectUrl);
                        }

                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }

                    AuthService.setCurrentUser(data.user, data.user.roles, data.token);
                    $rootScope.$broadcast('hitmands.auth:login.resolved', {
                        data: data,
                        status: status,
                        headers: headers,
                        config: config,
                        statusText: statusText
                    });
                })
                .error(function(data, status, headers, config, statusText){
                    $scope.error    = data.message;
                    $scope.loading  = false;

                    AuthService.setCurrentUser(null, null, null);
                    $rootScope.$broadcast('hitmands.auth:login.rejected', {
                        data: data,
                        status: status,
                        headers: headers,
                        config: config,
                        statusText: statusText
                    });
                })
            ;
        };

        var watchCredentials = function(newVal, oldVal){
            if (newVal === oldVal) {
                return;
            }

            $scope.error = false;
        };

        // Clear error message when user starts typing again
        $scope.$watch('username', watchCredentials);
        $scope.$watch('password', watchCredentials);
        $scope.$watch('rememberMe', watchCredentials);
    })

    .controller('RegisterCtrl', function($scope, $http, $timeout, $window) {
        $scope.error = null;
        $scope.success = false;
        $scope.loading = false;

        $scope.register = function($event){
            $event.preventDefault();
            $event.stopPropagation();

            $scope.loading = true;
            $scope.error   = null;

            $http({
                method: 'POST',
                url: Routing.generate('fos_user_registration_register', { '_format': 'json' }),
                data: $.param({
                    name         : this.name,
                    email        : this.email,
                    plainPassword: this.plainPassword,
                    companyName  : this.companyName
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
                .success(function(data, status){
                    if (data.redirectUrl !== undefined) {
                        $scope.error   = false;
                        $scope.success = true;
                        $window.location.href = data.redirectUrl;
                        $scope.loading = false;
                    } else {
                        $scope.loading = false;
                    }
                })
                .error(function(data, status){
                    $scope.error    = data.message;
                    $scope.loading  = false;
                })
            ;
        };
    })

    .controller('ZymUsersCtrl', ['$scope', '$state', '$stateParams', 'userProvider', function($scope, $state, $stateParams, userProvider){
        $scope.$state     = $state;
        $scope.page       = $stateParams.page;
        $scope.limit      = $stateParams.limit;
        $scope.totalItems = undefined;
        $scope.loading    = true;

        $scope.users = [];

        $scope.pageChanged = function(){
            if (!$scope.loading) {
                $state.go('admin.people.users', { page: $scope.page, limit: $scope.limit });
            } else {
                $scope.page = $stateParams.page;
            }
        };

        userProvider.loadUsers($scope.page, $scope.limit)
            .success(function(data){
                $scope.users = data.users.items;
                $scope.totalItems = data.users.totalItems;
                $scope.loading = false;
            })
            .error(function(data){
                $scope.loading = false;
            })
        ;
    }])

    .controller('ZymGroupsCtrl', ['$scope', '$stateParams', 'groupProvider', function($scope, $stateParams, groupProvider){
        $scope.page = $stateParams.page;
        $scope.limit = $stateParams.limit;
        $scope.totalItems = 0;
        $scope.loading = true;

        $scope.groups = [];

        $scope.pageChanged = function(){
            if (!$scope.loading) {
                $state.go('admin.people.groups', { page: $scope.page, limit: $scope.limit });
            } else {
                $scope.page = $stateParams.page;
            }
        };

        groupProvider.loadGroups($stateParams.page, $stateParams.limit)
            .success(function(data){
                $scope.groups = data.groups.items;
                $scope.totalItems = data.groups.totalItems;
                $scope.loading = false;
             })
            .error(function(data){
                $scope.loading = false;
            })
        ;
    }])
;
