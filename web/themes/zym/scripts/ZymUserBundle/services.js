angular.module('ZymUserBundle')
    .provider('userProvider', [function(){
        this.$get = ['$http', function($http){
            var users         = [];

            return {
                /**
                 * Load users
                 *
                 * @returns {*}
                 */
                loadUsers: function(page, limit){
                    page = page || 1;
                    limit = limit || 50;

                    var promise = $http({
                        method: 'GET',
                        url: Routing.generate('zym_user_users', {
                            'page': page,
                            'limit': limit,
                            '_format': 'json'
                        })
                    });

                    promise
                        .success(function(data, status){
                            //organizations = data.organizations.items;
                        })
                        .error(function(data){
                            // Messenger().post({
                            //     message: 'An error occurred refreshing organizations: ' + data.message,
                            //     type:    'error'
                            // });
                        })
                    ;

                    return promise;
                }
            };
        }];
    }])
    .provider('groupProvider', [function(){
        this.$get = ['$http', function($http){
            var groups         = [];

            return {
                /**
                 * Load groups
                 *
                 * @returns {*}
                 */
                loadGroups: function(page, limit){
                    page = page || 1;
                    limit = limit || 50;

                    var promise = $http({
                        method: 'GET',
                        url: Routing.generate('zym_user_groups', {
                            'page': page,
                            'limit': limit,
                            '_format': 'json'
                        })
                    });

                    promise
                        .success(function(data, status){
                            //organizations = data.organizations.items;
                        })
                        .error(function(data){
                            // Messenger().post({
                            //     message: 'An error occurred refreshing organizations: ' + data.message,
                            //     type:    'error'
                            // });
                        })
                    ;

                    return promise;
                }
            };
        }];
    }])
;
