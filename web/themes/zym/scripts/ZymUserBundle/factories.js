angular.module('ZymUserBundle')
    .factory('sessionRecoverer', ['$q', '$injector', function($q, $injector) {
        return {
            responseError: function(response) {
                // Session has expired
                if (response.status == 403){
                    var $rootScope   = $injector.get('$rootScope');
                    var $state       = $injector.get('$state');
                    var $stateParams = $injector.get('$stateParams');
                    var $http        = $injector.get('$http');
                    var $timeout     = $injector.get('$timeout');
                    var AuthService  = $injector.get('AuthService');
                    var deferred     = $q.defer();

                    // We use login method that logs the user in using the current credentials and
                    // returns a promise

                    // Check if user is logged in
                    if (!AuthService.isUserLoggedIn()) {
                        // Redirect to login
                        $state.go('login');
                    } else {
                        // User appears to be authenticated, but does not have permissions
                        // Check if user is still logged in
                        AuthService.fetchLoggedUser()
                            .then(function(){
                                // User session expired, login again
                                if (!AuthService.isUserLoggedIn()) {
                                    // Redirect to login
                                    $state.go('login');
                                } else {
                                    // User is logged in, but doesn't have permissions
                                    // Show forbidden instead
                                    onResolved();
                                    onRejected();

                                    return $q.reject(response);
                                }
                            })
                        ;
                    }

                    var onResolved = $rootScope.$on('hitmands.auth:login.resolved', deferred.resolve);
                    var onRejected = $rootScope.$on('hitmands.auth:login.rejected', deferred.reject);

                    // Lets not queue up too many requests if the user never sucessfully logs in
                    // We just reject after 60s
                    $timeout(deferred.reject, 1000 * 60);

                    // When the session recovered, make the same backend call again and chain the request
                    return deferred.promise.then(function() {
                        onResolved();
                        onRejected();

                        return $http(response.config);
                    }, function(){
                        onResolved();
                        onRejected();

                        return $q.reject(response);
                    });
                }

                return $q.reject(response);
            }
        };
    }])
;
