angular.module('ZymUserBundle')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {
        $locationProvider
            .html5Mode(true)
            .hashPrefix('!');

        // For any unmatched url, redirect to /state1
        //$urlRouterProvider.otherwise("/login");

        // Now set up the states
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: Routing.generate('fos_user_security_login', { '_format': 'ajax' })
            })

            .state('logout', {
                url: '/logout',
                templateUrl: Routing.generate('fos_user_security_logout', { '_format': 'ajax' })
            })

            .state('register', {
                url: '/signup',
                templateUrl: Routing.generate('fos_user_registration_register', { '_format': 'ajax' })
            })

            .state('account', {
                abstract: true,
                url: '/account',
                template: '<ui-view layout="column" flex />'
            })

            .state('account.resetting', {
                url: '/forgot',
                templateUrl: Routing.generate('zym_user_resetting_request', { '_format': 'ajax' })
            })

            .state('account.resetting_check_email', {
                url: '/forgot/check-email',
                templateUrl: Routing.generate('zym_user_resetting_check_email', { '_format': 'ajax' })
            })

            .state('account.resetting_reset', {
                url: '/forgot/reset/{token}',
                templateUrl: function(stateParams){
                    return Routing.generate('zym_user_resetting_reset', { 'token': 'reset-token', '_format': 'ajax' }).replace('reset-token', stateParams.token);
                }
            })

            .state('admin', {
                abstract: true,
                url: '/admin',
                template: '<ui-view />'
            })

            .state('admin.people', {
                abstract: true,
                templateUrl: Routing.generate('zym_user_users_layout', { '_format': 'ajax' })
            })

            .state('admin.people.users', {
                url: '/users?page&limit',
                params: {
                    page: '1',
                    limit: '50'
                },
                templateUrl: Routing.generate('zym_user_users', { '_format': 'ajax' })
            })

            .state('admin.people.users.add', {
                url: '/add',
                templateUrl: Routing.generate('zym_user_users_add', { '_format': 'ajax' })
            })

            .state('admin.people.users.edit', {
                url: '/{id:int}/edit',
                templateUrl: function(stateParams) {
                    return Routing.generate('zym_user_users_edit', { 'id': stateParams.id, '_format': 'ajax' })
                }
            })

            .state('admin.people.users.show', {
                url: '/{id:int}',
                templateUrl: function(stateParams) {
                    return Routing.generate('zym_user_users_show', { 'id': stateParams.id, '_format': 'ajax' })
                }
            })

            .state('admin.people.groups', {
                url: '/groups?page&limit',
                params: {
                    page: '1',
                    limit: '50'
                },
                templateUrl: Routing.generate('zym_user_groups', { '_format': 'ajax' })
            })

            .state('admin.people.groups.add', {
                url: '/add',
                templateUrl: Routing.generate('zym_user_groups_add', { '_format': 'ajax' })
            })

            .state('admin.people.groups.edit', {
                url: '/{id:int}/edit',
                templateUrl: function(stateParams) {
                    return Routing.generate('zym_user_groups_edit', { 'id': stateParams.id, '_format': 'ajax' })
                }
            })

            .state('admin.people.groups.show', {
                url: '/{id:int}',
                templateUrl: function(stateParams) {
                    return Routing.generate('zym_user_groups_show', { 'id': stateParams.id, '_format': 'ajax' })
                }
            })
        ;
    }])

    .config(['AuthServiceProvider', '$cookiesProvider', '$httpProvider', function(AuthServiceProvider, $cookiesProvider, $httpProvider){
        // Set API ENDPOINTS
        AuthServiceProvider.useRoutes({
            login:  Routing.generate('fos_user_security_check', { '_format': 'json' }),
            logout: Routing.generate('fos_user_security_logout', { '_format': 'json' }),
            fetch:  Routing.generate('zym_user_security_me', { '_format': 'json' })
        });

        AuthServiceProvider.parseHttpAuthData(function(data, headers, statusCode) {
            // Get the session id from the cookie provider since $cookies isn't available in .config()
            var sessionToken = $cookiesProvider.$get()['session'] || 'none';

            // Check if user is not logged in anymore
            if (typeof(data.user) == 'string') {
                // user: "anon." means that the user isn't logged in anymore

                return {
                    user: null,
                    token: null,
                    authLevel: 0
                };
            }

            return {
                user: data.user,
                token: sessionToken,
                authLevel: data.user.roles
            };
        });

        $httpProvider.interceptors.push('sessionRecoverer');
    }])
;
