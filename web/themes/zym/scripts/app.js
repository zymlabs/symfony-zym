angular.module('ZymLabsChartsBundle', ['ui.router', 'ui.bootstrap', 'ngMaterial'])
    .run(['$rootScope', function($rootScope){
        "use strict";

        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
            console.log(error.message);

            var msg = 'A ' + (error.status ? error.status : '500') + ' error occurred while loading the next page. We\'re sorry about the issue. Please try again.';

            if (error.status == 403) {
                msg = 'Permission Denied: Sorry, you don\'t have permissions to access this.';
            }

            Messenger().post({
                message: msg,
                type:    'error'
            });
        });
    }])
;


angular.module('app', ['ui.bootstrap', 'ui.gravatar', 'angular-loading-bar', 'ngMessages', 'ngAnimate', 'ngAria', 'ngMaterial', 'ZymUserBundle', 'zym.resque'])
;
