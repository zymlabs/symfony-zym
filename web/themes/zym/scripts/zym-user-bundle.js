angular.module('ZymUserBundle', ['ui.router', 'ngAnimate', 'ngCookies', 'hitmands.auth'])
    .run(['$rootScope', '$state', 'AuthService', function($rootScope, $state, AuthService){
        // Get user info when page is reloaded
        if (!AuthService.isUserLoggedIn()) {
            AuthService.fetchLoggedUser();
        }

        // Handle when user is not logged in or session expired
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
            if (error.status == 401) {
                event.preventDefault();

                // User is unauthenticated, so go to login
                $state.go('login');
            } else if (error.status == 403) {
                event.preventDefault();


                if (!AuthService.isUserLoggedIn()) {
                    $state.go('login');
                }

                // User appears to be authenticated, but does not have permissions
                // Check if user is still logged in
                AuthService.fetchLoggedUser()
                    .then(function(){
                        if (!AuthService.isUserLoggedIn()) {
                            // Redirect to login
                            $state.go('login');
                        } else {
                            // User is logged in, but doesn't have permissions
                            // Show forbidden instead
                            alert('Insufficient Permissions');
                        }
                    })
                ;
            }
        });
    }])
;
