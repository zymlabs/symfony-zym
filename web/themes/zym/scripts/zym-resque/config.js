angular.module('zym.resque')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {
        $locationProvider
            .html5Mode(true)
            .hashPrefix('!');

        // For any unmatched url, redirect to /state1
        //$urlRouterProvider.otherwise("/login");

        // Now set up the states
        $stateProvider
            .state('admin.resque', {
                url: '/resque',
                templateUrl: Routing.generate('zym_resque', { '_format': 'ajax' })
            })

            .state('admin.resque.queues', {
                url: '/queues',
                templateUrl: Routing.generate('zym_resque_queues', { '_format': 'ajax' })
            })

            .state('admin.resque.workers', {
                url: '/workers',
                templateUrl: Routing.generate('zym_resque_workers', { '_format': 'ajax' })
            })

            .state('admin.resque.failures', {
                url: '/failures',
                templateUrl: Routing.generate('zym_resque_failures', { '_format': 'ajax' })
            })
        ;
    }])
;