angular.module('zym.resque')
    .controller('ZymResqueDashboardCtrl', ['$scope', function($scope){

    }])

    .controller('ZymResqueFailuresCtrl', function($scope, $http, $timeout) {
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };

        $scope.totalServerItems = 0;

        $scope.pagingOptions = {
            pageSizes: [50, 100, 250],
            pageSize: 50,
            currentPage: 1
        };

        $scope.setPagingData = function(data, page, pageSize){
            $scope.myData = data.items;
            $scope.totalServerItems = data.totalItems;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.getPagedDataAsync = function (pageSize, page, searchText) {
            $timeout(function () {
                var data;

                if (searchText) {
                    var ft = searchText.toLowerCase();

                    $http.get(Routing.generate('zym_resque_failures', { '_format': 'json' }) + '?page=' + page + '&limit=' + pageSize).success(function (largeLoad) {
                        data = largeLoad.failures.filter(function(item) {
                            return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                        });

                        $scope.setPagingData(data, page, pageSize);
                    });
                } else {
                    $http.get(Routing.generate('zym_resque_failures', { '_format': 'json' }) + '?page=' + page + '&limit=' + pageSize)
                        .success(function (response) {
                            $scope.setPagingData(response.failures, page, pageSize);
                        })
                    ;
                }
            }, 100);
        };

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
            }
        }, true);

        $scope.selectedRow = false;

        $scope.gridOptions = {
            data: 'myData',
            columnDefs: [
                { field:'failedAt',       displayName: 'Failed At' },
                { field:'payload.id',     displayName: 'ID' },
                { field:'worker',         displayName: 'Worker',    visible: false },
                { field:'queue',          displayName: 'Queue',     visible: false },
                { field:'payload.class',  displayName: 'Class',     visible: false },
                { field:'exception',      displayName: 'Exception', visible: false }
            ],

            enableGridMenu: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnResizing: true,
            enablePaging: false,
            enablePinning: false,

            //footerTemplate: '<div data-ng-show="showFooter" class="ngFooterPanel" data-ng-class="{\'ui-widget-content\' : jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" data-ng-style="footerStyle()">\
            //                    <div class="ngPagerContainer" style="float: right; margin-top: 10px;" data-ng-show="enablePaging" data-ng-class="{\'ngNoMultiSelect\': !multiSelect}">\
            //                        <div class="hidden-xs hidden-sm hidden-md" style="float:left; margin-right: 10px;" class="ngRowCountPicker">\
            //                            <span style="float: left; margin-top: 3px;" class="ngLabel">{{i18n.ngPageSizeLabel}}</span>\
            //                            <select style="margin-top: 4px; width: 55px; margin-left: 5px;" data-ng-model="pagingOptions.pageSize" >\
            //                                <option data-ng-repeat="size in pagingOptions.pageSizes">{{size}}</option>\
            //                            </select>\
            //                        </div>\
            //                        <div style="float:left; margin-right: 10px; line-height:25px;" class="ngPagerControl" style="float: left; min-width: 135px;">\
            //                            <button class="btn btn-default btn-xs" data-ng-click="pageToFirst()" data-ng-disabled="cantPageBackward()" title="{{i18n.ngPagerFirstTitle}}"><span class="glyphicon glyphicon-fast-backward"></span></button>\
            //                            <button class="btn btn-default btn-xs" data-ng-click="pageBackward()" data-ng-disabled="cantPageBackward()" title="{{i18n.ngPagerPrevTitle}}"><span class="glyphicon glyphicon-backward"></button>\
            //                            Page <input class="ngPagerCurrent " min="1" max="{{ maxPages() < 1 ? 1 : maxPages() }}" type="text" style="width:40px; height: 24px; margin-top: 1px; line-height: 1em; padding: 0 4px;" data-ng-model="pagingOptions.currentPage"/> of {{ maxPages() < 1 ? 1 : maxPages() }}\
            //                            <button class="btn btn-default btn-xs" data-ng-click="pageForward()" data-ng-disabled="cantPageForward()" title="{{i18n.ngPagerNextTitle}}"><span class="glyphicon glyphicon-forward"></button>\
            //                            <button class="btn btn-default btn-xs" data-ng-click="pageToLast()" data-ng-disabled="cantPageToLast()" title="{{i18n.ngPagerLastTitle}}"><span class="glyphicon glyphicon-fast-forward"></button>\
            //                        </div>\
            //                    </div>\
            //                    <div class="ngTotalSelectContainer" >\
            //                        <div class="ngFooterTotalItems" data-ng-class="{\'ngNoMultiSelect\': !multiSelect}" >\
            //                            <span class="ngLabel">{{i18n.ngTotalItemsLabel}} {{maxRows()}}</span><span data-ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span>\
            //                        </div>\
            //                        <div class="ngFooterSelectedItems" data-ng-show="multiSelect">\
            //                            <span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span>\
            //                        </div>\
            //                    </div>\
            //                </div>',
            filterOptions: $scope.filterOptions,

            headerRowHeight: 36,
            multiSelect: false,

            pagingOptions: $scope.pagingOptions,
            showColumnMenu: true,
            showFilter: true,
            showGridFooter:true,
            rowHeight: 37,

            totalServerItems: 'totalServerItems',

            onRegisterApi: function(gridApi){
                //set gridApi on scope
                $scope.gridApi = gridApi;

                // Update selected row when row selection changes
                gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    if (row.isSelected) {
                        $scope.selectedRow = row;
                    } else {
                        $scope.selectedRow = false;
                    }
                });

                // CellNav only highlights a cell on arrow key nav. Lets do the whole row
                gridApi.cellNav.on.navigate($scope,function(newRowCol, oldRowCol){
                    $scope.gridApi.selection.selectRow(newRowCol.row.entity);
                });
            }
        };
    })
;